# Spanish translations for kcm_mobileshell.po package.
# Copyright (C) 2022 This file is copyright:
# This file is distributed under the same license as the plasma-mobile package.
#
# Automatically generated, 2022.
# SPDX-FileCopyrightText: 2022, 2023 Eloy Cuadra <ecuadra@eloihr.net>
msgid ""
msgstr ""
"Project-Id-Version: kcm_mobileshell\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-01-23 00:38+0000\n"
"PO-Revision-Date: 2023-11-07 21:52+0100\n"
"Last-Translator: Eloy Cuadra <ecuadra@eloihr.net>\n"
"Language-Team: Spanish <kde-l10n-es@kde.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 23.08.2\n"

#: ui/main.qml:18
#, kde-format
msgid "Shell"
msgstr "Intérprete de órdenes"

#: ui/main.qml:27
#, kde-format
msgid "General"
msgstr "General"

#: ui/main.qml:33 ui/VibrationForm.qml:18 ui/VibrationForm.qml:25
#, kde-format
msgid "Shell Vibrations"
msgstr "Vibraciones del intérprete de órdenes"

#: ui/main.qml:41
#, kde-format
msgid "Animations"
msgstr "Animaciones"

#: ui/main.qml:42
#, kde-format
msgid "If this is off, animations will be reduced as much as possible."
msgstr ""
"Si está desactivado, las animaciones se reducirán tanto como sea posible."

#: ui/main.qml:53
#, kde-format
msgid "Navigation Panel"
msgstr "Panel de navegación"

#: ui/main.qml:75
#, kde-format
msgid "Always show keyboard toggle"
msgstr "Mostrar siempre el selector de teclado"

#: ui/main.qml:76
#, kde-format
msgid ""
"Whether to always show the keyboard toggle button on the navigation panel."
msgstr ""
"Controla si se debe mostrar siempre el botón de cambio de teclado en el "
"panel de navegación."

#: ui/main.qml:87
#, kde-format
msgid "Action Drawer"
msgstr "Cajón de acciones"

#: ui/main.qml:93
#, kde-format
msgctxt "Pinned action drawer mode"
msgid "Pinned Mode"
msgstr "Modo anclado"

#: ui/main.qml:94
#, kde-format
msgctxt "Expanded action drawer mode"
msgid "Expanded Mode"
msgstr "Modo expandido"

#: ui/main.qml:98 ui/QuickSettingsForm.qml:18 ui/QuickSettingsForm.qml:71
#, kde-format
msgid "Quick Settings"
msgstr "Preferencias rápidas"

#: ui/main.qml:106
#, kde-format
msgid "Top Left Drawer Mode"
msgstr "Modo de cajón superior izquierdo"

#: ui/main.qml:107
#, kde-format
msgid "Mode when opening from the top left."
msgstr "Modo al abrir desde la parte superior izquierda."

#: ui/main.qml:132
#, kde-format
msgid "Top Right Drawer Mode"
msgstr "Modo de cajón superior derecho"

#: ui/main.qml:133
#, kde-format
msgid "Mode when opening from the top right."
msgstr "Modo al abrir desde la parte superior derecha."

#: ui/QuickSettingsForm.qml:55
#, kde-format
msgctxt "@action:button"
msgid "Hide"
msgstr "Ocultar"

#: ui/QuickSettingsForm.qml:55
#, kde-format
msgctxt "@action:button"
msgid "Show"
msgstr "Mostrar"

#: ui/QuickSettingsForm.qml:76
#, kde-format
msgid ""
"Customize the order of quick settings in the pull-down panel and hide them."
msgstr ""
"Personalizar el orden de los ajustes rápidos en el panel desplegable y "
"ocultarlos."

#: ui/QuickSettingsForm.qml:99
#, kde-format
msgid "Disabled Quick Settings"
msgstr "Preferencias rápidas desactivadas"

#: ui/QuickSettingsForm.qml:104
#, kde-format
msgid "Re-enable previously disabled quick settings."
msgstr "Volver a activar las preferencias rápidas anteriormente desactivadas."

#: ui/VibrationForm.qml:26
#, kde-format
msgid "Whether to have vibrations enabled in the shell."
msgstr ""
"Controla si se deben activar las vibraciones en el intérprete de órdenes."

#: ui/VibrationForm.qml:39
#, kde-format
msgid "Vibration Duration"
msgstr "Duración de la vibración"

#: ui/VibrationForm.qml:40
#, kde-format
msgid "How long shell vibrations should be."
msgstr "Duración que deben tener las vibraciones del intérprete de órdenes."

#: ui/VibrationForm.qml:42
#, kde-format
msgctxt "Long duration"
msgid "Long"
msgstr "Larga"

#: ui/VibrationForm.qml:43
#, kde-format
msgctxt "Medium duration"
msgid "Medium"
msgstr "Media"

#: ui/VibrationForm.qml:44
#, kde-format
msgctxt "Short duration"
msgid "Short"
msgstr "Corta"

#: ui/VibrationForm.qml:68
#, kde-format
msgid ""
"Keyboard vibrations are controlled separately in the keyboard settings "
"module."
msgstr ""
"Las vibraciones del teclado se controlan de forma separada en el módulo de "
"ajustes del teclado."

#~ msgid "Gesture-only Mode"
#~ msgstr "Modo de solo gestos"

#~ msgid "Whether to hide the navigation panel."
#~ msgstr "Controla si se debe ocultar el panel de navegación."

#~ msgid "Vibration Intensity"
#~ msgstr "Intensidad de la vibración"

#~ msgid "How intense shell vibrations should be."
#~ msgstr ""
#~ "Intensidad que deben tener las vibraciones del intérprete de órdenes."

#~ msgctxt "Low intensity"
#~ msgid "Low"
#~ msgstr "Baja"

#~ msgctxt "Medium intensity"
#~ msgid "Medium"
#~ msgstr "Media"

#~ msgctxt "High intensity"
#~ msgid "High"
#~ msgstr "Alta"

#~ msgid "Task Switcher"
#~ msgstr "Selector de tareas"

#~ msgid "Show Application Previews"
#~ msgstr "Mostrar vistas previas de aplicaciones"

#~ msgid "Turning this off may help improve performance."
#~ msgstr "Desactivar esto puede ayudar a mejorar el rendimiento."
